import React from "react";
import "./App.css";
import Header from "./components/Header";
import Home from "./components/Home";
import MedicalAppointments from "./features/medical_appointments/components/MedicalAppointments";
import { Route, Switch } from "react-router-dom";

function App() {
  return (
    <div className="App">
      <Header />
      <Switch>
        <Route exact path="/" component={Home} />
        <Route
          path="/configure_medical_appointments"
          component={MedicalAppointments}
        />
      </Switch>
    </div>
  );
}

export default App;
