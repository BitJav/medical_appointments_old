import { configureStore } from "@reduxjs/toolkit";
import hourRangesReducer from "../features/medical_appointments/hourRangesSlice";

export default configureStore({
  reducer: {
    hourRanges: hourRangesReducer,
  },
});
