export const hours = [
  "8:00 a.m.",
  "8:15 a.m.",
  "8:30 a.m.",
  "8:45 a.m.",
  "9:00 a.m.",
  "9:15 a.m.",
  "9:30 a.m.",
  "9:45 a.m.",
  "10:00 a.m.",
  "10:15 a.m.",
  "10:30 a.m.",
  "10:45 a.m.",
  "11:00 a.m.",
  "11:15 a.m.",
  "11:30 a.m.",
  "11:45 a.m.",
  "00:00 a.m.",
  "00:15 a.m.",
  "00:30 a.m.",
  "00:45 a.m.",
  "1:00 p.m.",
  "1:15 p.m.",
  "1:30 p.m.",
  "1:45 p.m.",
  "2:00 p.m.",
  "2:15 p.m.",
  "2:30 p.m.",
  "2:45 p.m.",
  "3:00 p.m.",
  "3:15 p.m.",
  "3:30 p.m.",
  "3:45 p.m.",
  "4:00 p.m.",
  "4:15 p.m.",
  "4:30 p.m.",
  "4:45 p.m.",
  "5:00 p.m.",
  "5:15 p.m.",
  "5:30 p.m.",
  "5:45 p.m.",
  "6:00 p.m.",
  "6:15 p.m.",
  "6:30 p.m.",
  "6:45 p.m.",
  "7:00 p.m.",
  "7:15 p.m.",
  "7:30 p.m.",
  "7:45 p.m.",
];

let idCount = 0;

function createHourOptions() {
  let hourOptions = {};
  hours.forEach((hour) => {
    hourOptions[idCount] = hour;
    idCount++;
  });
  return hourOptions;
}

export const hourOptions = createHourOptions();

export const hourCount = hours.length;

export function getHourByIndex(index) {
  return hourOptions[index];
}
