import React, { useState } from "react";
import DayOfTheWeek from "./DayOfTheWeek";
import HourRanges from "./HourRanges";
import voca from "voca";
import { useSelector, useDispatch } from "react-redux";
import {
  addHourRange,
  updateHourRange,
  deleteHourRange,
  selectHourRanges,
} from "../hourRangesSlice";
import "../medicalAppointments.scss";

function MedicalAppointments() {
  const [isDaySelected, setIsDaySelected] = useState(false);
  const [selectedDay, setSelectedDay] = useState(null);

  const hourRanges = useSelector(selectHourRanges);
  const dispatch = useDispatch();

  function clickHandler({ target }) {
    if (target.classList.contains("selected-day")) {
      target.classList.remove("selected-day");
      setIsDaySelected(false);
      setSelectedDay(null);
    } else {
      if (isDaySelected) {
        let currentSelectedDay = document.getElementsByClassName(
          "selected-day"
        )[0];
        currentSelectedDay.classList.remove("selected-day");
      }
      target.classList.add("selected-day");
      setIsDaySelected(true);
      setSelectedDay(target.innerText);
    }
  }

  function addHourRangeHandler() {
    const day = voca.lowerCase(selectedDay);
    const dayRanges = hourRanges[day];
    const length = dayRanges.length;
    let newRange = {};
    if (length === 0) {
      newRange = {
        startIndex: 0,
        endIndex: 1,
      };
    } else {
      const newStartIndex = dayRanges[length - 1].endIndex + 1;
      newRange = {
        startIndex: newStartIndex,
        endIndex: newStartIndex + 1,
      };
    }

    dispatch(addHourRange({ day, newRange }));
  }

  function rangeHandler(rangeIndex) {
    return ({ target }) => {
      const day = voca.lowerCase(selectedDay);
      const valueType = target.name;
      const valueIndexType = valueType + "Index";

      const valueIndex = parseInt(target.value);

      let newRange = {};
      if (valueType === "start") {
        let endIndex = hourRanges[day][rangeIndex].endIndex;
        if (valueIndex > endIndex) {
          endIndex = valueIndex + 1;
        }
        newRange = {
          [valueIndexType]: valueIndex,
          endIndex,
        };
      } else {
        newRange = {
          ...hourRanges[day][rangeIndex],
          [valueIndexType]: valueIndex,
        };
      }

      dispatch(updateHourRange({ day, index: rangeIndex, newRange }));
    };
  }

  return (
    <>
      <h1>Configure medical appointments for Dr. .....</h1>
      <div className="to-center">
        <div className="appointments-configuration">
          <div className="days-of-the-week">
            <DayOfTheWeek
              dayOfTheWeek="Mon"
              hourRanges={hourRanges.mon}
              selectedDay={selectedDay}
              clickHandler={clickHandler}
            />
            <DayOfTheWeek
              dayOfTheWeek="Tue"
              hourRanges={hourRanges.tue}
              selectedDay={selectedDay}
              clickHandler={clickHandler}
            />
            <DayOfTheWeek
              dayOfTheWeek="Wed"
              hourRanges={hourRanges.wed}
              selectedDay={selectedDay}
              clickHandler={clickHandler}
            />
            <DayOfTheWeek
              dayOfTheWeek="Thr"
              hourRanges={hourRanges.thr}
              selectedDay={selectedDay}
              clickHandler={clickHandler}
            />
            <DayOfTheWeek
              dayOfTheWeek="Fri"
              hourRanges={hourRanges.fri}
              selectedDay={selectedDay}
              clickHandler={clickHandler}
            />
          </div>
          {isDaySelected && (
            <>
              <button
                className="add-hour-range-button"
                onClick={addHourRangeHandler}
              >
                Add hour range
              </button>
              <HourRanges
                hourRanges={hourRanges[voca.lowerCase(selectedDay)]}
                rangeHandler={rangeHandler}
                deleteHandler={() =>
                  dispatch(
                    deleteHourRange({ day: voca.lowerCase(selectedDay) })
                  )
                }
              />
            </>
          )}
        </div>
      </div>
    </>
  );
}

export default MedicalAppointments;
