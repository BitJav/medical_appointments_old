import React from "react";
import { hourCount, hours } from "../timeUtils";

let idCount = 0;
let hourOptions = {};

function initializeHourOptions() {
  hours.forEach((hour) => {
    hourOptions[idCount] = hour;
    idCount++;
  });
}

function getStartingIndex(previousSelectIndex, selectType) {
  return previousSelectIndex
    ? previousSelectIndex + 1
    : selectType === "end"
    ? 1
    : 0;
}

function SelectHourInput({
  name,
  label,
  value,
  previousSelectIndex,
  selectHandler,
}) {
  initializeHourOptions();
  let options = [];
  let firstIndex = getStartingIndex(previousSelectIndex, name);
  const hoursCount = hours.length;
  let top = name === "start" ? hoursCount - 1 : hourCount;
  for (var i = firstIndex; i < top; i++) {
    const hour = hourOptions[i];
    options[i] = (
      <option key={i} id={i} value={i}>
        {hour}
      </option>
    );
  }
  return (
    <div className="hour-range-select">
      <label htmlFor={name}>{label}</label>
      <br></br>
      <select name={name} value={value} onChange={(e) => selectHandler(e)}>
        {options}
      </select>
    </div>
  );
}

export default SelectHourInput;
