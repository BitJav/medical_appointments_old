import React from "react";
import SelectHourInput from "./SelectHourInput";

function HourRanges({ hourRanges, rangeHandler, deleteHandler }) {
  let lastUsedIndexes = [];
  let hourRangesToShow = [];
  const hourRangesQuantity = Object.keys(hourRanges).length;

  for (let i = 0; i < hourRangesQuantity; i++) {
    const hourRange = hourRanges[i];
    const optionIndex = i === 0 ? undefined : hourRanges[i - 1].endIndex;
    hourRangesToShow[i] = (
      <div key={i} className="hour-range">
        <SelectHourInput
          name="start"
          label="Start"
          value={hourRange.startIndex}
          previousSelectIndex={optionIndex}
          selectHandler={rangeHandler(i)}
        />
        <SelectHourInput
          name="end"
          label="End"
          value={hourRange.endIndex}
          previousSelectIndex={hourRange.startIndex}
          selectHandler={rangeHandler(i)}
        />
        {i === hourRangesQuantity - 1 && (
          <button className="delete-hour-range-button" onClick={deleteHandler}>
            delete
          </button>
        )}
      </div>
    );
    lastUsedIndexes[i + 1] = hourRange.endIndex
      ? hourRange.endIndex
      : optionIndex + 2;
  }

  return <div className="hour-ranges">{hourRangesToShow}</div>;
}

export default HourRanges;
