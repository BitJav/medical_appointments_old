import React from "react";
import PropTypes from "prop-types";

function DayOfTheWeek({ dayOfTheWeek, clickHandler }) {
  return (
    <>
      <div className="day-of-the-week" onClick={clickHandler}>
        {dayOfTheWeek}
      </div>
    </>
  );
}

DayOfTheWeek.propTypes = {
  dayOfTheWeek: PropTypes.string.isRequired,
  clickHandler: PropTypes.func.isRequired,
};

export default DayOfTheWeek;
