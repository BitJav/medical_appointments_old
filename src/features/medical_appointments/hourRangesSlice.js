import { createSlice } from "@reduxjs/toolkit";
import { hourCount } from "./timeUtils";
import * as initialState from "../../common/initialState";

export const slice = createSlice({
  name: "hourRanges",
  initialState: initialState.hourRanges,
  reducers: {
    addHourRange: (state, action) => {
      const { day, newRange } = action.payload;
      state[day].push(newRange);
    },
    updateHourRange: (state, action) => {
      const { day, index, newRange } = action.payload;
      debugger;

      state[day][index] = newRange;
      const hourRangesQuantity = state[day].length;

      // If the modified hour range isn't the last one
      // update the following hour ranges if the end
      // of the modified one exceeds the start of the
      // following
      if (index < hourRangesQuantity) {
        // let hourRangeToCompare = newRange;
        for (let i = index + 1; i < hourRangesQuantity; i++) {
          let previousEndIndex = state[day][i - 1].endIndex;
          let currentStartIndex = state[day][i].startIndex;
          if (previousEndIndex >= currentStartIndex) {
            // If the previous end index exceeds the maximum
            // posible index (hourCount - 3) then remove all
            // hour ranges from the current until the end
            if (previousEndIndex > hourCount - 3) {
              state[day].splice(i);
              break;
            } else {
              let updatedRange = {
                startIndex: previousEndIndex + 1,
                endIndex: previousEndIndex + 2,
              };
              state[day][i] = updatedRange;
            }
          } else {
            break;
          }
        }
      }
    },
    deleteHourRange: (state, action) => {
      const { day } = action.payload;
      state[day].pop();
    },
  },
});

export const { addHourRange, updateHourRange, deleteHourRange } = slice.actions;

export const selectHourRanges = (state) => state.hourRanges;

export default slice.reducer;
