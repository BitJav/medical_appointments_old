import React from "react";

function Home() {
  return (
    <div className="home-greetings">
      Appointments' configuration and booking site!
    </div>
  );
}

export default Home;
