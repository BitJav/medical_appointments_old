import React from "react";
import { NavLink } from "react-router-dom";

function Header() {
  let activeStyle = { color: "#a4161a" };

  return (
    <nav className="header">
      <header className="App-header">
        <div className="middle">
          <NavLink className="nav-link" to="/" exact activeStyle={activeStyle}>
            Home
          </NavLink>
          {" | "}
          <NavLink
            className="nav-link"
            to="/configure_medical_appointments"
            activeStyle={activeStyle}
          >
            Configuration
          </NavLink>
        </div>
      </header>
    </nav>
  );
}

export default Header;
